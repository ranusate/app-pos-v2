<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//check_not_login();
		//check_admin();
		$this->load->model('M_Customer');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$user = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();
		$listSup = $this->M_Customer->getAll();
		$data = array(
			"header" => "Customer",
			"judul" => "Data Customer",
			"title" => 'Dashboard',
			"user" => $user,
			"customers" => $listSup
		);

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar');
		$this->load->view('customer/v_list_customer', $data);
		$this->load->view('templates/footer');

		//$this->load->view('dashboard', $data);
	}
	public function delete($id)
	{
		$this->M_Customer->delete($id);
		$error = $this->db->error();

		if ($error['code'] != 0) {
			echo "<script>
			alert('Data Tidak bisah dihapus ! sudah berelasi');
			</script>";
		} else {
			echo "<script>
			alert('Data berhasil dihapus');
			</script>";
		}
		echo "<script>
		window.location='" . site_url('supplier') . "';
			</script>";
	}

	public function tambah()
	{

		//	$data['title'] = 'submenu Management';
		$user = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();

		$Customer = new stdClass();
		$Customer->id = null;
		$Customer->nama = null;
		$Customer->jk = null;
		$Customer->no_tlpn = null;
		$Customer->alamat = null;
		$data = array(
			//"page" => "customer/v_edit_customer",
			"title" => 'Tambah Customer',
			"apa" => "Tambah",
			"user" => $user,
			"row" => $Customer
		);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('customer/v_edit_customer', $data);
		$this->load->view('templates/footer');
		//$this->load->view('dashboard', $data);
	}
	public function update($id)
	{
		$user = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();


		$query = $this->M_Customer->get($id);
		if ($query->num_rows() > 0) {
			$Customer = $query->row();
			$data = array(
				//	"page" => "customer/v_edit_customer",
				"apa" => "Update",
				"title" => 'Update Customer',
				'row' => $Customer,
				"user" => $user,

			);
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar');
			$this->load->view('customer/v_edit_customer', $data);
			$this->load->view('templates/footer');

			//$this->load->view('dashboard', $data);
		} else {
			echo "<script>
			alert('Data Tidak ditemukan');
			window.location='" . base_url('customer') . "';
			</script>";
		}
	}


	public function proses()
	{

		$post = $this->input->post(null, true);
		if (isset($_POST['Tambah'])) {
			$this->M_Customer->insert($post);
		}
		if ($this->db->affected_rows() > 0) {
			echo "<script>
			alert('Data berhasil ditambah');
			window.location='" . base_url('customer') . "';
			</script>";
		} else if (isset($_POST['Update'])) {
			$this->M_Customer->update($post);
		}
		if ($this->db->affected_rows() > 0) {
			echo "<script>
				alert('Data berhasil diupdate');
				window.location='" . base_url('customer') . "';
				</script>";
		}
	}
}