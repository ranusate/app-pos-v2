<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// check_not_login();
		//check_admin();
		$this->load->model('M_supplier');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$user = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();

		$listSup = $this->M_supplier->get()->result();
		$data = array(
			"user" => $user,
			"judul" => "Supplier",
			"title" => "Data Supplier",
			"suppliers" => $listSup
		);


		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar');
		$this->load->view('supplier/v_list_supplier', $data);
		$this->load->view('templates/footer');
		//$this->load->view('dashboard', $data);
	}


	public function delete($id)
	{
		$this->M_supplier->delete($id);
		$error = $this->db->error();

		if ($error['code'] != 0) {
			echo "<script>
			alert('Data Tidak bisah dihapus ! sudah berelasi');
			</script>";
		} else {
			echo "<script>
			alert('Data berhasil dihapus');
			</script>";
		}
		echo "<script>
		window.location='" . site_url('supplier') . "';
			</script>";
	}


	public function tambah()
	{
		$user = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();


		$supplier = new stdClass();
		$supplier->id = null;
		$supplier->name = null;
		$supplier->no_tlpn = null;
		$supplier->alamat = null;
		$supplier->decripsi = null;

		$data = array(
			"user" => $user,
			"judul" => "Supplier",
			"title" => "Data Supplier",
			"apa" => "add",
			"row" => $supplier
		);

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar');
		$this->load->view('supplier/v_edit_supplier', $data);
		$this->load->view('templates/footer');

		// $this->load->view('dashboard', $data);
	}
	public function update($id)
	{

		$user = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();


		$query = $this->M_supplier->get($id);
		if ($query->num_rows() > 0) {
			$supplier = $query->row();
			$data = array(
				"user" => $user,
				"judul" => "Supplier",
				"title" => "Data Supplier",
				"apa" => "update",
				'row' => $supplier
			);
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar');
			$this->load->view('supplier/v_edit_supplier', $data);
			$this->load->view('templates/footer');
			// $this->load->view('dashboard', $data);
		} else {
			echo "<script>
			alert('Data Tidak ditemukan');
			window.location='" . base_url('supplier') . "';
			</script>";
		}
	}



	public function proses_tambah()
	{
		$post = $this->input->post(null, true);
		if (isset($_POST['add'])) {
			$this->M_supplier->insert($post);
		}
		if ($this->db->affected_rows() > 0) {
			echo "<script>
			alert('Data berhasil ditambah');
			window.location='" . base_url('supplier') . "';
			</script>";
		} else if (isset($_POST['update'])) {
			$this->M_supplier->update($post);
		}
		if ($this->db->affected_rows() > 0) {
			echo "<script>
				alert('Data berhasil diupdate');
				window.location='" . base_url('supplier') . "';
				</script>";
		}
	}
}