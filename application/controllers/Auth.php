<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library("form_validation");
	}
	public function index()
	{
		if ($this->session->userdata('email')) {
			redirect('user');
		}
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Pasword', 'trim|required');
		if ($this->form_validation->run() == false) {

			$data['title'] = 'Login Page';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		} else {
			//validasi login
			$this->_login();
		}
	}


	private function _login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');


		$user = $this->db->get_where('user', ['email' => $email])->row_array();
		//var_dump($user);
		//die;
		if ($user) {
			//jika usernya aktif
			if ($user['is_active'] == 1) {
				// cek passwordny
				if (password_verify($password, $user['password'])) {
					//siapin data
					$data = [
						'email' => $user['email'],
						'role_id' => $user['role_id']
					];

					$this->session->set_userdata($data);
					if ($user['role_id'] == 1) {

						redirect('admin');

					} else {
						redirect('user');
					}
				} else {

					$this->session->set_flashdata(
						'message',
						'<div class="alert alert-success" role="alert">Password salah</div>'
					);
					redirect('auth');
				}
			} else {
				$this->session->set_flashdata(
					'message',
					'<div class="alert alert-danger" role="alert">Email Belum Diactivasi</div>'
				);
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata(
				'message',
				'<div class="alert alert-danger" role="alert">Email Belum Terdaftar</div>'
			);
			redirect('auth');
		}
	}

	public function registration()
	{
		if ($this->session->userdata('email')) {
			redirect('user');
		}

		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules(
			'email',
			'Email',
			'required|trim|valid_email|is_unique[user.email]',
			[
				'is_unique' => 'Email ini sudah terdaftar!'
			]
		);
		$this->form_validation->set_rules(
			'password1',
			'Password',
			'required|trim|min_length[3]|matches[password2]',
			[
				'matches' => 'Password tidak sama!',
				'min_length' => 'Password minimal 3 karakter!'
			]
		);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'User Registration';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/registration');
			$this->load->view('templates/auth_footer');
		} else {

			$email  = $this->input->post('email', true);
			$data =
				[
					'name' => htmlspecialchars($this->input->post('name', true)),
					'email' => htmlspecialchars($email),
					'image' => 'defaulth.jpg',
					'password' => password_hash(
						$this->input->post('password1'),
						PASSWORD_DEFAULT
					),
					'role_id' => 2,
					'is_active' => 0,
					'date_created' => time()

				];

			$token = base64_encode(random_bytes(32));

			$user_token = [
				'email' => $email,
				'token' => $token,
				'date_created' => time()
			];

			$this->db->insert('user', $data);
			$this->db->insert('user_token', $user_token);
			//mengirim email mengaktifkan fitur email

			$this->_sendEmail($token, 'verify');

			$this->session->set_flashdata(
				'message',
				'<div class="alert alert-success" role="alert">Silakan aktivasi email anda. </div>'
			);
			redirect('auth');
			//echo 'data berhasil';
		}
	}



	private function _sendEmail($token, $type)
	{
		$config = array(
			'useragent' => 'CodeIgniter',
			'protocol' => 'smtp',
			'mailpath' => '/usr/sbin/sendmail',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_user' => 'test9ong@gmail.com',
			'smtp_pass' => "15112607",
			'smtp_port' => 465,
			'smtp_keepalive' => TRUE,
			'smtp_crypto' => 'ssl',
			'wordwrap' => TRUE,
			'wrapchars' => 76,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'validate' => TRUE,
			'crlf' => "\r\n",
			'newline' => "\r\n",
		);

		$this->load->library('email', $config);
		$this->email->initialize($config);
		$this->email->from('test9ong@gmail.com', 'Radianus');
		$this->email->to($this->input->post('email'));

		if ($type == 'verify') {
			$this->email->subject('Account Verifycation');
			$this->email->message('Click this link to verify you account :
				<a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Activate</a>');
		} else if ($type == 'forgot') {
			$this->email->subject('Reset Password');
			$this->email->message('Click this link to reset you password :
				<a href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Reset Password</a>');
		}



		if ($this->email->send()) {
			return true;
		} else {
			echo $this->email->print_debugger();
			die;
		}
	}

	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_where('user', ['email' => $email])->row_array();
		if ($user) {

			$user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();
			if ($user_token) {
				if (time() - $user_token['date_created'] < (60 * 60 * 24)) {

					$this->db->set('is_active', 1);
					$this->db->where('email', $email);
					$this->db->update('user');
					$this->db->delete('user_token', ['email' => $email]);
					$this->session->set_flashdata(
						'message',
						'<div class="alert alert-success" role="alert">' . $email . 'has been activated!
						Pleace login.</div>'
					);
					redirect('auth');
				} else {
					$this->db->delete('user', ['email' => $email]);
					$this->db->delete('user_token', ['email' => $email]);
					$this->session->set_flashdata(
						'message',
						'<div class="alert alert-danger" role="alert">Activasi gagal ,Token expired</div>'
					);
					redirect('auth');
				}
			} else {
				$this->session->set_flashdata(
					'message',
					'<div class="alert alert-danger" role="alert">Activasi gagal ,Token salah</div>'
				);
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata(
				'message',
				'<div class="alert alert-danger" role="alert">Activasi gagal ,Email salah</div>'
			);
			redirect('auth');
		}
	}


	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role_id');
		$this->session->set_flashdata(
			'message',
			'<div class="alert alert-success" role="alert">Anda berhasil logout!</div>'
		);
		redirect('auth');
	}

	public function blocked()
	{
		$this->load->view('auth/blocked');
	}

	public function forgotPassword()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		if ($this->form_validation->run() == false) {
			$data['title'] = 'Forgot Password';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/forgot_password');
			$this->load->view('templates/auth_footer');
		} else {
			$email = $this->input->post('email');
			$user = $this->db->get_where('user', ['email' => $email, 'is_active' => 1])->row_array();
			if ($user) {
				$token = base64_encode(random_bytes(32));
				$user_token = ['email' => $email, 'token' => $token, 'date_created' => time()];
				$this->db->insert('user_token', $user_token);
				$this->_sendEmail($token, 'forgot');

				$this->session->set_flashdata(
					'message',
					'<div class="alert alert-succes" role="alert">Silakan cek email untuk reset password!</div>'
				);
				redirect('auth/forgotPassword');
			} else {
				$this->session->set_flashdata(
					'message',
					'<div class="alert alert-danger" role="alert">Email tidak terdaftar!</div>'
				);
				redirect('auth/forgotPassword');
			}
		}
	}

	public function resetpassword()
	{
		if (!$this->session->userdata('reset_email')) {
			redirect('auth');
		}


		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_where('user', ['email' => $email])->row_array();

		if ($user) {

			$user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();

			if ($user_token) {
				$this->session->set_userdata('reset_email', $email);
				$this->changePassword();
			} else {
				$this->session->set_flashdata(
					'message',
					'<div class="alert alert-success" role="alert">Reset Password gagal,Token tidak valid!</div>'
				);
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata(
				'message',
				'<div class="alert alert-success" role="alert">Reset Password gagal,Email tidak valid!</div>'
			);
			redirect('auth');
		}
	}
	public function changePassword()
	{
		$this->form_validation->set_rules(
			'password1',
			'Password',
			'required|trim|min_length[3]|matches[password2]',
			[
				'matches' => 'Password tidak sama!',
				'min_length' => 'Password minimal 3 karakter!'
			]
		);
		$this->form_validation->set_rules('password2', 'Repeat Password', 'required|trim|matches[password1]');
		if ($this->form_validation->run() == false) {
			$data['title'] = 'Change Password';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/change_password');
			$this->load->view('templates/auth_footer');
		} else {
			$password = password_hash($this->input->post('password1'), PASSWORD_DEFAULT);
			$email = $this->session->userdata('reset_email');

			$this->db->set('password', $password);
			$this->db->where('email', $email);
			$this->db->update('user');
			$this->session->unset_userdata('reset_email');
			$this->session->set_flashdata(
				'message',
				'<div class="alert alert-success" role="alert">Password berhasil diubah,Silakan login!</div>'
			);
			redirect('auth');
		}
	}
}