<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-code"></i>
        </div>
        <div class="sidebar-brand-text mx-3">POS<sup></sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- tempilkan menu yang diakes oleh admin -->

    <?php
    $role_id = $this->session->userdata('role_id');
    $queryMenu = "select `user_menu`.`id`, `menu`
                FROM `user_menu` join `user_access_menu`
				ON `user_menu`.`id` = `user_access_menu`.`menu_id`

				where `user_access_menu`.`role_id` = $role_id

                ORDER BY `user_access_menu`.`menu_id` ASC
				";



    $menu = $this->db->query($queryMenu)->result_array();

    ?>

    <!-- loping menu -->
    <?php foreach ($menu as $m) : ?>
    <div class="sidebar-heading">
        <?= $m['menu']; ?>
    </div>
    <!-- sub Menu -->


    <?php
        $menuId = $m['id'];
        $querySubMenu = " select *
                            FROM `user_sub_menu` join `user_menu`
							ON `user_sub_menu`.`menu_id` = `user_menu`.`id`

                            where `user_sub_menu`.`menu_id` = $menuId
                            and `user_sub_menu`.`is_active` = 1
                            ";
        $subMenu = $this->db->query($querySubMenu)->result_array();

        ?>


    <!-- //loping submenu -->

    <?php foreach ($subMenu as $sbm) : ?>
    <?php if ($title == $sbm['title']) : ?>
    <li class="nav-item active ">
        <?php else : ?>
        <?php endif; ?>


    <li class="nav-item">
        <a class="nav-link pb-0 collapsed" href="<?= base_url($sbm['url']); ?>">

            <i class="<?= $sbm['icon']; ?>"></i>

            <span><?= $sbm['title']; ?></span></a>
    </li>

    <?php endforeach; ?>
    <!-- Divider -->
    <hr class="sidebar-divider mt-3">
    <?php endforeach; ?>

    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('auth/logout'); ?>">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Logout</span></a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <!-- Divider -->
    <!-- Heading -->
    <!-- Nav Item - Pages Collapse Menu -->
    <!-- Divider -->

    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>