<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="<?= base_url() ?>supplier/tambah" class="btn btn-primary">
                <i class="fas fa-plus"></i> Supplier
            </a> </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="table1" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>No Telphon</th>
                            <th>Alamat</th>
                            <th>Deskripsi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>

                    </tfoot>
                    <tbody>

                        <?php $no = 1;
						foreach ($suppliers as $user) { ?>
                        <tr>
                            <td style="width:5%;"> <?= $no++ ?></td>
                            <td class="username"><?= $user->nama ?></td>
                            <td class="name"><?= $user->no_tlpn ?></td>
                            <td><?= $user->alamat ?></td>
                            <td><?= $user->decripsi ?></td>
                            <td>
                                <a href="<?= site_url("supplier/update/$user->id") ?>" class="btn btn-sm btn-warning">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <!-- <a href="<?= site_url("supplier/delete/$user->id") ?>"
                                 onclick="return confirm('Anda yakin menghapus data ?')" class="btn btn-sm btn-danger">
                                 <i class="fas fa-trash"></i>
							 </a> -->

                                <a href="#modaldel" data-toggle="modal"
                                    onclick="$('#modaldel #formdel').attr('action','<?= site_url('supplier/delete/' . $user->id); ?>')"
                                    class="btn btn-sm btn-danger">
                                    <i class="fas fa-trash"></i>
                                </a>


                            </td>
                        </tr>
                        <?php
						} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>

<div class="modal fade" id="modaldel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold text-primary text-center" id="exampleModalLabel">Hapus Data
                    Suplier
                </h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="modal-title">
                    yakin hapus data ini?
                </h5>
            </div>
            <div class="modal-footer">
                <form id="formdel" action="" method="POST">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger" type="submit" id="linkHapus" href="">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

</div>