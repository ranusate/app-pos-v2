<div class="row">
    <div class="col-lg-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    Tambah Supplier
                </h6>
            </div>
            <div class="card-body">
                <?php
				?>
                <form method="post" class="form" action="<?= base_url('supplier/proses_tambah') ?>">
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?= $row->id ?>">
                        <input type="text" value="<?= $row->nama ?>" class="form-control" id="sup_nam" name="sup_nam"
                            placeholder="Nama Supplier" required>
                    </div>
                    <div class="form-group">
                        <input type="number" name="no" value="<?= $row->no_tlpn ?>" class="form-control" id="no_tlpn"
                            placeholder="No Telphon" required>
                    </div>
                    <div class="form-group">
                        <textarea value="<?= $row->alamat ?>" placeholder="Alamat" name="alamat" class="form-control">
						</textarea>
                    </div>
                    <div class="form-group">
                        <textarea value="<?= $row->decripsi ?>" class="form-control" name="decripsi"
                            placeholder="Deskripsi" required></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger">Close</button>
                        <button type="submit" name="<?= $apa ?>" class="btn btn-primary btn-flat">Tambah</button>
                    </div>

                </form>










            </div>
        </div>
    </div>
</div>