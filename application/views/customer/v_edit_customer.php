<div class="row">
    <div class="col-lg-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    Tambah customer
                </h6>
            </div>
            <div class="card-body">
                <?php
				?>
                <form method="post" class="form" action="<?= base_url('customer/proses') ?>">
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?= $row->id ?>">
                        <input type="text" value="<?= $row->nama ?>" class="form-control" id="nama" name="nama"
                            placeholder="Nama customer" required>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="jk" name="jk" placeholder="Jk">
                            <option value="">Jenis Kelamin</option>
                            <option value="L" <?= $row->jk == 'L' ? 'selected' : '' ?>> Laki-laki</option>
                            <option value="P" <?= $row->jk == 'P' ? 'selected' : '' ?>>Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="number" name="no_tlpn" value="<?= $row->no_tlpn ?>" class="form-control"
                            id="no_tlpn" placeholder="No Telphon" required>
                    </div>
                    <div class="form-group">
                        <input type="text" value="<?= $row->alamat ?>" placeholder="Alamat" name="alamat"
                            class="form-control">
                        </input>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger">Close</button>
                        <button type="submit" name="<?= $apa ?>" class="btn btn-primary btn-flat">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>