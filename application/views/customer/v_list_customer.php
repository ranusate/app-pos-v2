 <!-- Page Heading -->
 <div class="container-fluid">
     <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <a href="<?= base_url() ?>customer/tambah" class="btn btn-primary">
                 <i class="fas fa-plus"></i> Customer
             </a> </div>
         <div class="card-body">
             <div class="table-responsive">
                 <table class="table table-bordered" id="table1" width="100%" cellspacing="0">
                     <thead>
                         <tr>
                             <th>#</th>
                             <th>Nama</th>
                             <th>Jenis Kelamin</th>
                             <th>No Telphon</th>
                             <th>Alamat</th>
                             <th>Action</th>
                         </tr>
                     </thead>
                     <tfoot>

                     </tfoot>
                     <tbody>

                         <?php $no = 1;
							foreach ($customers as $user) { ?>
                         <tr>
                             <td style="width:5%;"><?= $no++ ?></td>
                             <td><?= $user->nama ?></td>
                             <td><?= $user->jk ?></td>
                             <td><?= $user->no_tlpn ?></td>
                             <td><?= $user->alamat ?></td>
                             <td>
                                 <a href="<?= site_url("customer/update/$user->id") ?>" class="btn btn-sm btn-warning">
                                     <i class="fas fa-edit"></i>
                                 </a>
                                 <a href="<?= site_url("customer/delete/$user->id") ?>"
                                     onclick="return confirm('Anda yakin menghapus data ?')"
                                     class="btn btn-sm btn-danger">
                                     <i class="fas fa-trash"></i>
                                 </a>

                             </td>
                         </tr>
                         <?php
							} ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>
 <!-- /.container-fluid -->
 </div>

 <!--
modal delete -->


 <!-- Modal -->
 <div class="modal fade" id="tambahUser" tabindex="-1" role="dialog" aria-labelledby="tambahUserTitle"
     aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title font-weight-bold text-primary text-center" id="tambahUserTitle">
                     </i>Tambah User
                     Baru
                 </h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <?php //echo validation_errors();
				?>
             <form method="post" action="<?= base_url('user/tambah') ?>">
                 <div class="modal-body">
                     <div class="form-group">
                         <input type="text" value="<?= set_value('fullname') ?>" class="form-control" id="name"
                             name="fullname" placeholder="Nama">
                         <?= form_error('fullname'); ?>
                     </div>
                 </div>
                 <div class=" modal-body">
                     <div class="form-group">
                         <input type="text" value="<?= set_value('username') ?>" class="form-control" id="username"
                             name="username" placeholder="Username">

                         <?= form_error('username'); ?>
                     </div>
                 </div>
                 <div class="modal-body">
                     <div class="form-group">
                         <input type="password" value="<?= set_value('password') ?>" class="form-control" id="password"
                             name="password" placeholder="Password">

                         <?= form_error('password'); ?>
                     </div>
                 </div>
                 <div class="modal-body">
                     <div class="form-group">
                         <input type="password" class="form-control" value="<?= set_value('passconf') ?>" id="password"
                             name="passconf" placeholder="Confirmasi password">

                         <?= form_error('passconf'); ?>
                     </div>
                 </div>
                 <div class="modal-body">
                     <div class="form-group">
                         <textarea class="form-control" id="alamat" name="alamat"
                             placeholder="Alamat"><?= set_value('alamat') ?></textarea>
                         <?= form_error('alamat'); ?>
                     </div>
                 </div>

                 <div class="modal-body">
                     <div class="form-group">
                         <select class="form-control" id="level" name="level" placeholder="Level">
                             <option value="">Pilih Level</option>
                             <option value="1" <?= set_value('level') == 1 ? "selected" : null ?>>Admin</option>
                             <option value="2" <?= set_value('level') == 2 ? "selected" : null ?>>Kasir</option>
                         </select>

                         <?= form_error('level'); ?>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-primary">Tambah</button>
                 </div>
             </form>
         </div>
     </div>
 </div>
 </div>
 </div>